package io.niceday.common.coffee.common.maker;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Target {

	private Class<?> clazz;    // class
	private String   packages; // backend package
	private String   api;      // backend api
	private String   author;   // author
	private String   path;     // frontend path
}